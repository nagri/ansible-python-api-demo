$(document).ready(function(){


  $("#submit-button").on("click", function(e) {
      e.preventDefault();

      $('#username_label').css("display","none");
      $('#username_syntax_label').css("display","none");
      $('#checkedValueGroups_label').css("display","none");

      if($("#txtUsername").val() == null || $("#txtUsername").val() == ''){
        $('#username_label').css("display","block");
        return false
      }

     
      var linux_user_name = $("#txtUsername").val();
      var linux_username =  /^[a-z][a-z0-9]*$/;
      if(linux_username.test(linux_user_name)){
      }else{
        $('#username_syntax_label').css("display","block");
        return false
      }

       var checkedValuesGroups = $('.group-list:checkbox:checked').map(function() {
        return this.value;
        }).get();

      if (checkedValuesGroups == null || checkedValuesGroups == ""){
          $('#checkedValueGroups_label').css("display","block");
          return false
        }


      var userDetails = {
        checkedValuesGroups : checkedValuesGroups,
        native_linux_user : linux_user_name,
      }

      $.ajax({
           type: "POST",
           url: "/create-user",
           dataType: 'json',
           data: JSON.stringify(userDetails, null, '\t'),
           contentType: 'application/json;charset=UTF-8',
           success: function(data){    
              console.log(data);
              if(data.results.success == false){
                alert(data.results.error);
              }else{
                alert("Added");
              }
           },
           error: function(error) {
                console.log(error);
                alert('Some Error');
            }
    });
  });

});