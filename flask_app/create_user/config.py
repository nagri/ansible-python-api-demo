import os

SECRET_KEY = os.urandom(128)
STATIC_ROOT = None

INVENTORY_FILE = "INVENTORY/production.ini"
SERVER = "http://localhost:5001"
