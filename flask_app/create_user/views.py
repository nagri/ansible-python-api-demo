from flask import render_template, request, jsonify
from create_user import app
import ansible.inventory
import json
import requests


INVENTORY_FILE = app.config.get('INVENTORY_FILE')
SERVER = app.config.get('SERVER')


@app.route('/', methods=['GET'])
def index():
    ansible_inventory = ansible.inventory.Inventory(INVENTORY_FILE)
    group_list = [g.name for g in ansible_inventory.get_groups()]
    return render_template('index.html', group_list=group_list)


@app.route('/create-user', methods=['POST'])
def create():
    """Add/Update User in DB and Servers"""
    try:
        data = json.loads(request.data)
        native_linux_user = data['native_linux_user']
        if native_linux_user.upper() == 'KONFILARITY':
            return jsonify(results={
                           "error": native_linux_user+" Name is Not Allowed",
                           "success": False, })
        groups_list = data['checkedValuesGroups']
        send_data = {'method': 'add_user',
                     'add_data': {
                            'native_linux_user': native_linux_user,
                            'groups_list': list(groups_list),
                            }
                     }
        send_data = json.dumps(send_data)
        output = requests.post(app.config.get('SERVER'),
                               data=send_data,
                               )
    except:
            return jsonify(
                success="Error Occured"
                )
    return jsonify(
        results={
            "user": native_linux_user,
            "status_code": output.status_code,
            "result": json.loads(output.text),
            "success": True
        }
    )
