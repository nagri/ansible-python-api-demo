import os

SECRET_KEY = os.urandom(128)
STATIC_ROOT = None

INVENTORY_FILE = 'INVENTORY/production.ini'
PLAYBOOK = 'LIBRARY/create-user-playbook.yml'
