# -*- coding: utf-8 -*-

from flask import request, jsonify
import ansible.inventory
import json
from config import INVENTORY_FILE
from tasks import ansible_add_user
from server import app


@app.route("/", methods=['POST'])
def king_slayer():
    """Execute specified command in a valid server/host"""
    result = False
    data = json.loads(request.get_data())['add_data']
    native_linux_user = data['native_linux_user']
    groups_list = data['groups_list']
    data = {
            'native_linux_user': native_linux_user,
            'groups_list': groups_list,
            }
    result = add_user(data)
    return jsonify({"result": result,
                    "success": True, })


def add_user(data):
    try:
        native_linux_user = data['native_linux_user']
        ansible_inventory = ansible.inventory.Inventory(INVENTORY_FILE)
        groups_list = data['groups_list']
        for group in groups_list:
            ansible_add_user(native_linux_user, ansible_inventory, group)
        return True
    except:
        return False
