# -*- coding: utf-8 -*-

import ansible.playbook
from ansible import callbacks
from ansible import utils
from server.config import PLAYBOOK

stats = callbacks.AggregateStats()
playbook_cb = callbacks.PlaybookCallbacks(verbose=utils.VERBOSITY)
runner_cb = callbacks.PlaybookRunnerCallbacks(stats, verbose=utils.VERBOSITY)


def ansible_add_user(username, inventory, group):
    ansible.playbook.PlayBook(
        playbook=PLAYBOOK,
        callbacks=playbook_cb,
        runner_callbacks=runner_cb,
        stats=stats,
        inventory=inventory,
        extra_vars={
            'name': username,
            'group_name': group,
            'perform_task': 'create_user',
        }
    ).run()
    print "PROC", stats.processed
    print "FAIL", stats.failures
    print "OK  ", stats.ok
    print "DARK", stats.dark
    print "CHGD", stats.changed
    print "SKIP", stats.skipped
